var request = require('request');
var Q = require('q');
var moment = require('moment-timezone');

GetAllGames = function AllGames() {
    var defer = Q.defer();
    request.get({
        uri: "http://ls.betradar.com/ls/feeds/?/betradar/el/Europe:Helsinki/gismo/event_fullfeed",
        headers: {}
    },
    function(err, httpResponse, body) {
        if (err) {
            console.log(err);
            defer.reject(err);
        }
        var result = parse_data(JSON.parse(body));
        response = {
            time: moment().format('MM/DD/YYYY HH:mm:ss'),
            data: result
        }
        defer.resolve(response);
    })
    return defer.promise;
}

console.log();

function parse_data(data) {
    var games = [];
    var gamename = '';
    for (var sport in data.doc[0].data) {
        for (var tournament in data.doc[0].data[sport].realcategories) {
            // if ( data.doc[0].data[sport]._sid != 2 ) continue;
            gamename = data.doc[0].data[sport].name;
            for (var matches in data.doc[0].data[sport].realcategories[tournament].tournaments) {
                var tournamentname = data.doc[0].data[sport].realcategories[tournament].tournaments[matches].name;
                for (var match in data.doc[0].data[sport].realcategories[tournament].tournaments[matches].matches) {
                    var matchdetails = data.doc[0].data[sport].realcategories[tournament].tournaments[matches].matches[match];
                    var response = {
                        "Starting Time": matchdetails._dt.time,
                        "Starting Date": matchdetails._dt.date,
                        "Tournament_Name": tournamentname,
                        "Game_Type": matchdetails.roundname.name,
                        "Game": gamename,
                        "Status": matchdetails.status.name,
                        "GameClock": matchdetails.timeinfo ? secondsToHms(matchdetails.timeinfo.played) : null,
                        "Score": {
                            "Home": matchdetails.result.home,
                            "Away": matchdetails.result.away
                        },
                        "Teams": {
                            "Home": matchdetails.teams.home.name,
                            "Away": matchdetails.teams.away.name
                        }
                    };
                    games.push(response);
                };
            };
        };
    };
    return games;
}

function secondsToHms(d) {
	d = Number(d);
	var h = Math.floor(d / 3600);
	var m = Math.floor(d % 3600 / 60);
	var s = Math.floor(d % 3600 % 60);
	return ((h > 0 ? h + ":" + (m < 10 ? "0" : "") : "") + m + ":" + (s < 10 ? "0" : "") + s);
};

function GameStatus(status, name) {
    if (name == 'live') {
        return 'live';
    }
    switch(status) {
        case 'live':
            return 'live';
        case 'all':
            return true;
        case status == 0:
            return 'after';
        case status >= 100:
            return 'final';
        default:
            return true;
    }
};

module.exports = {
    GetAllGames : GetAllGames
};