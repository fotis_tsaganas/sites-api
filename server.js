var express = require('express');
var moment = require('moment-timezone');
var livescore = require('./services/livescore');
var betradar = require('./services/betradar');
var data = require('./data.js');
var app = express();
moment.tz.setDefault("Europe/Athens");

const IP = process.env.OPENSHIFT_NODEJS_IP || '127.0.0.1';
const PORT = process.env.OPENSHIFT_NODEJS_PORT || 8080;
const INTERVAL = 600000;

app.use(function (req, res, next) {
    res.removeHeader('X-Powered-By');
    var key = req.get('live-score-api-key');
    if (key === '21akcj^&%98ASyc9a*)CB09*ASdb9ya7dbcS)(AKJC89a*6as9c8') {
        next();
        return;
    }
    res.status(404).send('Cannot '+ req.method +' ' + req.originalUrl);
    console.log('Time:', moment().format('MM/DD/YYYY HH:mm:ss zz || ') + ' From: ' + req.connection.remoteAddress);
});

app.get('/api', function (req, res) {
    res.send('Api');
});

app.get('/api/betradar', function (req, res) {
    res.json(data.betradar.allgames);
});

app.get('/api/livescore', function (req, res) {
    res.json(data.livescore.games);
});

setInterval(function() {
    RefreshData();
}, INTERVAL);

app.listen(PORT, IP, function () {
    console.log('Listening on port: ' + PORT);
    RefreshData();
});

function RefreshData() {
    console.log("Refreshing... : " + moment().format('MM/DD/YYYY HH:mm:ss zz || '));
    // livescore.hitLivescore().then( function(body) {
    //     response = {
    //         time: moment().format('MM/DD/YYYY HH:mm:ss'),
    //         data: livescore.parseResponseBody(body)
    //     }
    //     data.livescore.games = response;
    // });

    // betradar.GetAllGames().then( function(body) {
    //     data.betradar.allgames = body;
    // });
}